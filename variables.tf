variable "region" {
  type = map
  default = {
  "subnet-pub" = "us-east-1a"
  "subnet-priv" = "us-east-1b"
}
}
variable "cidr_block_pub" {
    default = "192.182.0.0/16"
  
}
variable "cidr_block_subnet" {
    type = map
    default = {
       "cidr-block-subpub" = "192.182.0.0/24"
    
}
}